Project is made to facilitate adding students to assessment repos on gitlab. It finds users based on username and projects by ID.
Currently the permissions are set to reporter (20).

config.js is structured like this:

```javascript
module.exports = {
    PORT: 8000,
    gitlab_key: <PRIVATE_KEY_FROM_GIT_LAB>
}
```

The  post `/api/projects/users` endpoint is expecting a data object that resembles this: (shown in json format)
```json
{
	"userNames": [
		"userName1",
		"userName2",
		"userName3",
		"userName4"
	],
	"projects": [
		"1",
		"2",
		"3"
	],
	"expiration": "YYYY-MM-DD"
}
```
The endpoint will check that each username is a valid GitLab user, and will not add or modify a user if the user already has access to the project.

However, the endpoint will not check for valid ids of projects or that the PRIVATE KEY in the config.js file has appropriate permissions to add users to the repos. If any errors of this nature occur the server will reply with a 500 status code.