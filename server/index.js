const express = require('express')
    , axios = require('axios')

const app = express()
    , config = require('./config')
    , PORT = config.PORT
    , baseUrl = 'https://gitlab.com/api/v4';

app.use(express.json())

app.post('/api/projects/users', function (req, res) {
    let { userNames, projects, expiration } = req.body;
    let stack = [];

    userNames.forEach(userName => {
        stack.push(axios.get(`${baseUrl}/users?username=${userName}&private_token=${config.gitlab_key}`))
    });

    Promise.all(stack).then(resp=>{
        let data = resp.map(c=>c.data)
        let userNamesNotFound = []
        data.forEach((c, i)=>{
            if (c.length === 0) { userNamesNotFound.push(userNames[i]) }
        })
        console.log('Usernames not found: ', userNamesNotFound.length)

        let users = data.filter(c=>c.length === 1).map(c=>c[0])

        let projectStack = []
        projects.forEach(project => {
            projectStack.push( axios.get(`${baseUrl}/projects/${project}/users?private_token=${config.gitlab_key}`))
        })

        Promise.all(projectStack).then(projectsUsers=>{
            let projectData = projects.map((project, i)=>{
                let existing = projectsUsers[i].data.map(c=>c.id)
                return {
                    projectId: project,
                    existingUserIds: existing
                }
            })
            console.log('Project Stack:', projectData)

            let userStack = []
            projectData.forEach(project=>{
                users.forEach(user => {
                    if(!project.existingUserIds.includes(user.id)){
                        let postConfig = {
                            id: project,
                            user_id: user.id,
                            access_level: 20,
                            expires_at: expiration
                        }
                        userStack.push( axios.post(`${baseUrl}/projects/${project.projectId}/members?private_token=${config.gitlab_key}`, postConfig) )
                    }
                });                
            })
            Promise.all(userStack).then(resp=>{
                let log = projectData.map(p=>{
                    p.previouslyAdded = p.existingUserIds.map( id => users.find( u=>u.id === id )? users.find( u=>u.id === id ).username: null)
                    delete p.existingUserIds;
                    return p
                })
                let resObj = {
                    userNamesNotFound,
                    projectExceptions: log
                }
                console.log('response', resObj)
                res.status(200).send(resObj)
                //----------Response sent-----------
            }).catch(err=>{
                console.log(err)
                res.status(500).send(err)
            })
        }).catch(err=>{
            console.log(err)
            res.status(500).send(err)
        })
    }).catch(err=>{
        console.log(err)
        res.status(500).send(err)
    })
})

app.listen(PORT, console.log(`Listening on port: ${PORT}`))